![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# Webpack Examples

A nifty collection of webpack configs for easy later use. The code on this repo is licensed under CC0 so that it's painless to use them for almost any purpose.


## Examples:

- Injected CSS inside `<head>`: `./css-in-head`


## How to use/run these?

- Install [Node.js](https://nodejs.org/en/download/) and [preferably](https://medium.com/ryadel/yarn-vs-npm-vs-pnpm-in-2019-comparison-and-verdict-1357f41163ab) [Yarn](https://yarnpkg.com/) (or npm).
- `cd` into a directory and run `yarn install` or `npm install`
- To run on a server, watch for changes and launch on your webbrowser, run `yarn run server` or `npm run server`
